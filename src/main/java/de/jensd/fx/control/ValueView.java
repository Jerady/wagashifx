
package de.jensd.fx.control;

import de.jensd.fx.glyphs.weathericons.WeatherIcon;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.geometry.Pos;
import javafx.scene.control.Control;

/**
 *
 * @author Jens Deters (mail@jensd.de)
 */
public class ValueView extends Control {

    private StringProperty title;
    private StringProperty value;
    private StringProperty unit;
    private StringProperty glyphName;
    private StringProperty fontSize;
    private ObjectProperty<Number> glyphSize;
    private ObjectProperty<Pos> valueAlignment;
    private ObjectProperty<Pos> titleAlignment;

    public ValueView() {
        getStyleClass().addAll("root", "value-view");
        setValue("20.0");
        setUnit("\u2103");
        setFontSize("1em");
        setGlyphSize(48.0);
        setTitle("Temperature Outdoor");
        setGlyphName(WeatherIcon.THERMOMETER.name());
    }

    public ObjectProperty<Pos> titleAlignmentProperty() {
        if (titleAlignment == null) {
            titleAlignment = new SimpleObjectProperty<>(Pos.CENTER_LEFT);
        }
        return titleAlignment;
    }

    public Pos getTitleAlignment() {
        return titleAlignmentProperty().getValue();
    }

    public void setTitleAlignment(Pos titlePos) {
        titleAlignmentProperty().setValue(titlePos);
    }

    public ObjectProperty<Pos> valueAlignmentProperty() {
        if (valueAlignment == null) {
            valueAlignment = new SimpleObjectProperty<>(Pos.CENTER_RIGHT);
        }
        return valueAlignment;
    }

    public Pos getValueAlignment() {
        return valueAlignmentProperty().getValue();
    }

    public void setValueAlignment(Pos valuePos) {
        valueAlignmentProperty().setValue(valuePos);
    }

    public final StringProperty titleProperty() {
        if (title == null) {
            title = new SimpleStringProperty("");
        }
        return title;
    }

    public final String getTitle() {
        return titleProperty().getValue();
    }

    public final void setTitle(String value) {
        titleProperty().setValue(value);
    }

    public final StringProperty valueProperty() {
        if (value == null) {
            value = new SimpleStringProperty("");
        }
        return value;
    }

    public final String getValue() {
        return valueProperty().getValue();
    }

    public final void setValue(String value) {
        valueProperty().setValue(value);
    }

    public final StringProperty unitProperty() {
        if (unit == null) {
            unit = new SimpleStringProperty();
        }
        return unit;
    }

    public final String getUnit() {
        return unitProperty().getValue();
    }

    public final void setUnit(String unit) {
        unitProperty().setValue(unit);
    }

    public final StringProperty glyphNameProperty() {
        if (glyphName == null) {
            glyphName = new SimpleStringProperty();
        }
        return glyphName;
    }

    public final String getGlyphName() {
        return glyphNameProperty().getValue();
    }

    public final void setGlyphName(String glyphName) {
        glyphNameProperty().setValue(glyphName);
    }

    public final ObjectProperty<Number> glyphSizeProperty() {
        if (glyphSize == null) {
            glyphSize = new SimpleObjectProperty();
        }
        return glyphSize;
    }

    public final Number getGlyphSize() {
        return glyphSizeProperty().getValue();
    }

    public final void setGlyphSize(Number glyphSize) {
        glyphSizeProperty().setValue(glyphSize);
    }

    public final StringProperty fontSizeProperty() {
        if (fontSize == null) {
            fontSize = new SimpleStringProperty();
        }
        return fontSize;
    }

    public final String getFontSize() {
        return fontSizeProperty().getValue();
    }

    public final void setFontSize(String fontSize) {
        fontSizeProperty().setValue(fontSize);
    }

    @Override
    public String getUserAgentStylesheet() {
        return getClass().getResource(getClass().getSimpleName().toLowerCase() + ".css").toExternalForm();
    }

}
