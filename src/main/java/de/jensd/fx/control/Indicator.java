/* 
 * Copyright 2014 Jens Deters.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.jensd.fx.control;

import com.sun.javafx.css.converters.PaintConverter;
import de.jensd.fx.control.skin.IndicatorSkin;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ObservableValue;
import javafx.css.CssMetaData;
import javafx.css.PseudoClass;
import javafx.css.StyleableObjectProperty;
import javafx.css.StyleableProperty;
import javafx.scene.control.Control;
import javafx.scene.control.Skin;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;

/**
 *
 * @author Jens Deters (www.jensd.de)
 * @version 1.0.0
 * @since 14-10-2014
 */
public class Indicator extends Control {

    public enum Result {
        PASS, FAIL, INDETERMINDED
    };
    public static final Color DEFAULT_PASS_COLOR = Color.YELLOWGREEN;
    public static final Color DEFAULT_INDETERMINDED_COLOR = Color.YELLOW;
    public static final Color DEFAULT_FAIL_COLOR = Color.ORANGERED;
    private static final PseudoClass PASS_PSEUDO_CLASS = PseudoClass.getPseudoClass("pass");
    private static final PseudoClass INDETERMINDED_PSEUDO_CLASS = PseudoClass.getPseudoClass("indetermined");
    private static final PseudoClass FAIL_PSEUDO_CLASS = PseudoClass.getPseudoClass("fail");
    private ObjectProperty<Result> resultProperty;
    private ObjectProperty<Paint> passColorProperty;
    private ObjectProperty<Paint> indeterminedColorProperty;
    private ObjectProperty<Paint> failColorProperty;
    private BooleanProperty passProperty;

    public Indicator() {
        getStyleClass().add("indicator");
        passProperty().addListener((ObservableValue<? extends Boolean> ov, Boolean t, Boolean t1) -> {
            setResult(t1?Result.PASS:Result.FAIL);
        });
    }

    /*
     * Methods
     */
    public final Result getResult() {
        return resultProperty().get();
    }

    public final void setResult(Result result) {
        resultProperty().set(result);
    }

    public final ObjectProperty<Result> resultProperty() {
        if (null == resultProperty) {
            resultProperty = new SimpleObjectProperty(Result.INDETERMINDED) {
                @Override
                protected void invalidated() {
                    pseudoClassStateChanged(PASS_PSEUDO_CLASS, isResultPass());
                    pseudoClassStateChanged(FAIL_PSEUDO_CLASS, isResultFail());
                    pseudoClassStateChanged(INDETERMINDED_PSEUDO_CLASS, isResultIndetermined());
                }

                @Override
                public Object getBean() {
                    return this;
                }

                @Override
                public String getName() {
                    switch (resultProperty().get()) {
                        case PASS:
                            return "PASS";
                        case FAIL:
                            return "FAIL";
                        case INDETERMINDED:
                        default:
                            return "INDETERMINDED";
                    }
                }
            };
        }
        return resultProperty;
    }

    public final Boolean isPass() {
        return passProperty().get();
    }

    public final void setPass(Boolean value) {
        passProperty().set(value);
    }

    public final BooleanProperty passProperty() {
        if (null == passProperty) {
            passProperty = new SimpleBooleanProperty(Boolean.FALSE);
        }
        return passProperty;
    }

    /*
     * CSS Stylable Properties 
     */
    public final Paint getPassColor() {
        return null == passColorProperty ? DEFAULT_PASS_COLOR : passColorProperty.get();
    }

    public final void setPassColor(Paint value) {
        passColorProperty().set(value);
    }

    public final ObjectProperty<Paint> passColorProperty() {
        if (null == passColorProperty) {
            passColorProperty = new StyleableObjectProperty<Paint>(DEFAULT_PASS_COLOR) {
                @Override
                public CssMetaData getCssMetaData() {
                    return StyleableProperties.PASS_COLOR;
                }

                @Override
                public Object getBean() {
                    return Indicator.this;
                }

                @Override
                public String getName() {
                    return "passColor";
                }
            };
        }
        return passColorProperty;
    }

    public final Paint getFailColor() {
        return null == failColorProperty ? DEFAULT_FAIL_COLOR : failColorProperty.get();
    }

    public final void setFailColor(Paint value) {
        failColorProperty().set(value);
    }

    public final ObjectProperty<Paint> failColorProperty() {
        if (null == failColorProperty) {
            failColorProperty = new StyleableObjectProperty<Paint>(DEFAULT_FAIL_COLOR) {
                @Override
                public CssMetaData getCssMetaData() {
                    return StyleableProperties.FAIL_COLOR;
                }

                @Override
                public Object getBean() {
                    return Indicator.this;
                }

                @Override
                public String getName() {
                    return "failColor";
                }
            };
        }
        return failColorProperty;
    }

    public final Paint getIndeterminedColor() {
        return null == indeterminedColorProperty ? DEFAULT_INDETERMINDED_COLOR : indeterminedColorProperty.get();
    }

    public final void setIndeterminedColor(Paint value) {
        indeterminedColorProperty().set(value);
    }

    public final ObjectProperty<Paint> indeterminedColorProperty() {
        if (null == indeterminedColorProperty) {
            indeterminedColorProperty = new StyleableObjectProperty<Paint>(DEFAULT_INDETERMINDED_COLOR) {
                @Override
                public CssMetaData getCssMetaData() {
                    return StyleableProperties.INDETERMINDED_COLOR;
                }

                @Override
                public Object getBean() {
                    return Indicator.this;
                }

                @Override
                public String getName() {
                    return "indeterminedColor";
                }
            };
        }
        return indeterminedColorProperty;
    }

    public boolean isResultPass() {
        return resultProperty().get().equals(Result.PASS);
    }

    public boolean isResultFail() {
        return resultProperty().get().equals(Result.FAIL);
    }

    public boolean isResultIndetermined() {
        return resultProperty().get().equals(Result.INDETERMINDED);
    }

    /*
     * STYLE
     */
    @Override
    protected Skin createDefaultSkin() {
        return new IndicatorSkin(this);
    }

    @Override
    public String getUserAgentStylesheet() {
        return getClass().getResource(getClass().getSimpleName().toLowerCase() + ".css").toExternalForm();
    }

    private static class StyleableProperties {

        private static final CssMetaData<Indicator, Paint> PASS_COLOR =
                new CssMetaData<Indicator, Paint>("-pass-color", PaintConverter.getInstance(), DEFAULT_PASS_COLOR) {
            @Override
            public boolean isSettable(Indicator indicator) {
                return null == indicator.passColorProperty || !indicator.passColorProperty.isBound();
            }

            @Override
            public StyleableProperty<Paint> getStyleableProperty(Indicator indicator) {
                return (StyleableProperty) indicator.passColorProperty();
            }

            @Override
            public Color getInitialValue(Indicator indicator) {
                return (Color) indicator.getPassColor();
            }
        };
        private static final CssMetaData<Indicator, Paint> FAIL_COLOR =
                new CssMetaData<Indicator, Paint>("-fail-color", PaintConverter.getInstance(), DEFAULT_FAIL_COLOR) {
            @Override
            public boolean isSettable(Indicator indicator) {
                return null == indicator.failColorProperty || !indicator.failColorProperty.isBound();
            }

            @Override
            public StyleableProperty<Paint> getStyleableProperty(Indicator indicator) {
                return (StyleableProperty) indicator.failColorProperty();
            }

            @Override
            public Color getInitialValue(Indicator indicator) {
                return (Color) indicator.getFailColor();
            }
        };
        private static final CssMetaData<Indicator, Paint> INDETERMINDED_COLOR =
                new CssMetaData<Indicator, Paint>("-indetermined-color", PaintConverter.getInstance(), DEFAULT_INDETERMINDED_COLOR) {
            @Override
            public boolean isSettable(Indicator indicator) {
                return null == indicator.indeterminedColorProperty || !indicator.indeterminedColorProperty.isBound();
            }

            @Override
            public StyleableProperty<Paint> getStyleableProperty(Indicator indicator) {
                return (StyleableProperty) indicator.indeterminedColorProperty();
            }

            @Override
            public Color getInitialValue(Indicator indicator) {
                return (Color) indicator.getIndeterminedColor();
            }
        };
    }
}
