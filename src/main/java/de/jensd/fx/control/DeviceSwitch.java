/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.jensd.fx.control;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Control;

/**
 *
 * @author Jens Deters (mail@jensd.de)
 */
public class DeviceSwitch extends Control {

    private StringProperty title;
    private StringProperty onButtonText;
    private StringProperty offButtonText;
    private ObjectProperty<EventHandler<ActionEvent>> onSwitchOn;
    private ObjectProperty<EventHandler<ActionEvent>> onSwitchOff;

    public DeviceSwitch() {
        getStyleClass().addAll("device-switch");
    }

    public final StringProperty titleProperty() {
        if (title == null) {
            title = new SimpleStringProperty("");
        }
        return title;
    }

    public final String getTitle() {
        return titleProperty().getValue();
    }

    public final void setTitle(String value) {
        titleProperty().setValue(value);
    }

    public final StringProperty onButtonTextProperty() {
        if (onButtonText == null) {
            onButtonText = new SimpleStringProperty("ON");
        }
        return onButtonText;
    }

    public final String getOnButtonText() {
        return onButtonTextProperty().getValue();
    }

    public final void setOnButtonText(String value) {
        onButtonTextProperty().setValue(value);
    }

    public final StringProperty offButtonTextProperty() {
        if (offButtonText == null) {
            offButtonText = new SimpleStringProperty("OFF");
        }
        return offButtonText;
    }

    public final String getOffButtonText() {
        return offButtonTextProperty().getValue();
    }

    public final void setOffButtonText(String value) {
        offButtonTextProperty().setValue(value);
    }

    @Override
    public String getUserAgentStylesheet() {
        return getClass().getResource(getClass().getSimpleName().toLowerCase() + ".css").toExternalForm();
    }

    public final ObjectProperty<EventHandler<ActionEvent>> onSwitchOnProperty() {
        if (onSwitchOn == null) {
            onSwitchOn = new SimpleObjectProperty<>();
        }
        return onSwitchOn;
    }

    public final void setOnSwitchOn(EventHandler<ActionEvent> value) {
        onSwitchOnProperty().set(value);
    }

    public final EventHandler<ActionEvent> getOnSwitchOn() {
        return onSwitchOnProperty().get();
    }

    public final ObjectProperty<EventHandler<ActionEvent>> onSwitchOffProperty() {
        if (onSwitchOff == null) {
            onSwitchOff = new SimpleObjectProperty<>();
        }
        return onSwitchOff;
    }

    public final void setOnSwitchOff(EventHandler<ActionEvent> value) {
        onSwitchOffProperty().set(value);
    }

    public final EventHandler<ActionEvent> getOnSwitchOff() {
        return onSwitchOffProperty().get();
    }

}
