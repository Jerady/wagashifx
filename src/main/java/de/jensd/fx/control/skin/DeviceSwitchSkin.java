package de.jensd.fx.control.skin;

import de.jensd.fx.control.DeviceSwitch;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.SkinBase;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;

/**
 *
 * @author Jens Deters (mail@jensd.de)
 */
public class DeviceSwitchSkin extends SkinBase<DeviceSwitch> {

    private static final double PREFERRED_WIDTH = 300;
    private Label titleLabel;
    private Button onButton;
    private Button offButton;
    private HBox mainPane;

    public DeviceSwitchSkin(DeviceSwitch control) {
        super(control);
        init();
        initNodes();
        initStyleClasses();
        registerBindings();
    }

    private void init() {
        getSkinnable().setPrefWidth(PREFERRED_WIDTH);
    }
    

    private void initNodes() {
        titleLabel = new Label();
        onButton = new Button();
        offButton = new Button();
        Region spring = new Region();
        HBox.setHgrow(spring, Priority.ALWAYS);
        mainPane = new HBox();
        mainPane.setSpacing(0.0);
        mainPane.setAlignment(Pos.CENTER_LEFT);
        mainPane.getChildren().addAll(titleLabel, spring, onButton, offButton);
        getChildren().add(mainPane);
    }

    private void initStyleClasses() {
        titleLabel.getStyleClass().add("title");
        onButton.getStyleClass().add("on");
        offButton.getStyleClass().add("off");
    }
    
    private void registerBindings() {
        titleLabel.textProperty().bind(getSkinnable().titleProperty());
        onButton.textProperty().bind(getSkinnable().onButtonTextProperty());
        offButton.textProperty().bind(getSkinnable().offButtonTextProperty());
        onButton.onActionProperty().bind(getSkinnable().onSwitchOnProperty());
        offButton.onActionProperty().bind(getSkinnable().onSwitchOffProperty());
    }

}
