package de.jensd.fx.control.skin;

import de.jensd.fx.control.UVIndexView;
import java.util.Arrays;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.SkinBase;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

/**
 *
 * @author Jens Deters (mail@jensd.de)
 */
public class UVIndexViewSkin extends SkinBase<UVIndexView> {

    private static final double PREFERRED_WIDTH = 100;
    private static final double PREFERRED_TICK_SIZE = 30.0;
    private static final double OPACITY_LOW = 0.1;
    private static final double SPACING = 3.0;
    private static final double PADDING = 3.0;
    private static final double INDEX_LABEL_MIN_WIDTH = 22.0;

    private HBox mainBox;
    private HBox ticksBox;
    private GridPane mainPane;
    private Region[] ticks;
    private Label[] indexValueLabel;
    private double tickSize;
    private Text uvLabel;
    private Text indexLabel;

    public UVIndexViewSkin(UVIndexView control) {
        super(control);
        init();
        initNodes();
        initStyleClasses();
        registerBindings();
    }

    private void init() {
        getSkinnable().setPrefWidth(PREFERRED_WIDTH);
    }

    private void initNodes() {
        ticks = new Region[11];
        ticks[0] = createTick("-fx-background-color: rgb(36,135,2);");
        ticks[1] = createTick("-fx-background-color: rgb(36,135,2);");
        ticks[2] = createTick("-fx-background-color: rgb(246,226,9);");
        ticks[3] = createTick("-fx-background-color: rgb(246,226,9);");
        ticks[4] = createTick("-fx-background-color: rgb(246,226,9);");
        ticks[5] = createTick("-fx-background-color: rgb(244,67,5);");
        ticks[6] = createTick("-fx-background-color: rgb(244,67,5);");
        ticks[7] = createTick("-fx-background-color: rgb(205,0,16);");
        ticks[8] = createTick("-fx-background-color: rgb(205,0,16);");
        ticks[9] = createTick("-fx-background-color: rgb(205,0,16);");
        ticks[10] = createTick("-fx-background-color: rgb(87,47,188);");

        indexValueLabel = new Label[11];
        for (int i = 0; i < 10; i++) {
            indexValueLabel[i] = new Label("" + (i + 1));

        }
        indexValueLabel[10] = new Label("11+");

        ticksBox = new HBox();
        ticksBox.setPadding(new Insets(PADDING));
        ticksBox.setSpacing(SPACING);
        ticksBox.getChildren().addAll(Arrays.asList(ticks));

        VBox titleBox = new VBox();
        titleBox.setAlignment(Pos.CENTER);
        uvLabel = new Text("UV");
        VBox.setVgrow(uvLabel, Priority.ALWAYS);
        uvLabel.setStyle("-fx - font - weight: bold;");
        indexLabel = new Text("index");
        VBox.setVgrow(indexLabel, Priority.ALWAYS);
        titleBox.getChildren().addAll(uvLabel, indexLabel);

        //barBox.setStyle("-fx-background-color: gray;");
        HBox.setHgrow(ticksBox, Priority.ALWAYS);
        mainBox = new HBox();
        HBox.setHgrow(mainBox, Priority.ALWAYS);
        mainBox.getChildren().addAll(ticksBox);
        getChildren().add(mainBox);
        reset();
    }

    private void reset() {
        highlightTicks(-1);
    }

    private void highlightTicks(int uvIndex) {
        int index = uvIndex-1;
        if (index < 0) {
            for (Region tick : ticks) {
                tick.setOpacity(OPACITY_LOW);
            }
            return;
        } else if (index >= ticks.length) {
            index = 10;
        }
        for (int i = 0; i < index + 1; i++) {
            ticks[i].setOpacity(1.0);
        }
        for (int i = index + 1; i < ticks.length; i++) {
            ticks[i].setOpacity(OPACITY_LOW);
        }
    }

    private Region createTick(String style) {
        Region tick = new Region();
        tick.setStyle(style);
        tick.setMinHeight(PREFERRED_TICK_SIZE);
        tick.setMaxHeight(PREFERRED_TICK_SIZE);
        tick.setPrefHeight(PREFERRED_TICK_SIZE);
        HBox.setHgrow(tick, Priority.ALWAYS);
        return tick;
    }

    private void initStyleClasses() {
    }

    private void registerBindings() {
        getSkinnable().valueProperty().addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
            if (newValue.doubleValue() < 0) {
                highlightTicks(-1);
                return;
            }
            highlightTicks(newValue.intValue());
        });
        getSkinnable().widthProperty().addListener((ObservableValue<? extends Number> ov, Number t, Number t1) -> {
            resize();
        });
    }

    private void resize() {
        tickSize = (getSkinnable().getWidth() - (SPACING * 12)) / ticks.length;
        if (tickSize > 0) {
            for (Region tick : ticks) {
                tick.setMinHeight(tickSize);
                tick.setMaxHeight(tickSize);
                tick.setPrefHeight(tickSize);
            }
        }

    }
}
