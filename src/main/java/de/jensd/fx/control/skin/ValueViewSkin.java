package de.jensd.fx.control.skin;

import de.jensd.fx.control.ValueView;
import de.jensd.fx.glyphs.weathericons.WeatherIconView;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.Separator;
import javafx.scene.control.SkinBase;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;

/**
 *
 * @author Jens Deters (mail@jensd.de)
 */
public class ValueViewSkin extends SkinBase<ValueView> {

    private static final double PREFERRED_WIDTH = 300;
    private static final double PREFERRED_HEIGHT = 100;

    private Label titleLabel;
    private Separator titleSeparator;
    private Label valueLabel;
    private Label unitLabel;
    private WeatherIconView glyph;
    private VBox iconBox;
    private HBox titleBox;
    private HBox valueBox;
    private HBox centerBox;
    private VBox unitBox;
    private VBox mainBox;

    public ValueViewSkin(ValueView control) {
        super(control);
        init();
        initNodes();
        initStyleClasses();
        registerBindings();
    }

    private void init() {
        getSkinnable().setPrefWidth(PREFERRED_WIDTH);
        getSkinnable().setPrefHeight(PREFERRED_HEIGHT);
    }

    private void initNodes() {
        titleLabel = new Label("TITLE");
        glyph = new WeatherIconView();
        valueLabel = new Label("VALUE");
        unitLabel = new Label("UNIT");
        titleBox = new HBox();
        titleBox.getChildren().add(titleLabel);
        iconBox = new VBox();
        iconBox.getChildren().addAll(glyph);
        iconBox.setAlignment(Pos.CENTER);
        VBox.setVgrow(iconBox, Priority.NEVER);
        valueBox = new HBox();
        HBox.setHgrow(valueBox, Priority.ALWAYS);
        valueBox.getChildren().addAll(valueLabel);
        unitBox = new VBox();
        unitBox.getChildren().addAll(unitLabel);
        unitBox.setAlignment(Pos.CENTER);
        VBox.setVgrow(unitBox, Priority.NEVER);
        titleSeparator = new Separator();
        centerBox = new HBox();
        centerBox.setAlignment(Pos.CENTER_RIGHT);
        centerBox.getChildren().addAll(iconBox, valueBox, unitBox);
        HBox.setHgrow(centerBox, Priority.ALWAYS);
        mainBox = new VBox();
        VBox.setVgrow(mainBox, Priority.ALWAYS);
        mainBox.getChildren().addAll(titleBox, titleSeparator, centerBox);
        getChildren().add(mainBox);
        adjustFonts();
    }

    private void initStyleClasses() {
        titleLabel.getStyleClass().add("title");
        glyph.getStyleClass().add("icon");
        valueLabel.getStyleClass().add("value");
        unitLabel.getStyleClass().add("unit");
        titleBox.getStyleClass().add("title-box");
        iconBox.getStyleClass().add("icon-box");
        valueBox.getStyleClass().add("value-box");
        unitBox.getStyleClass().add("unit-box");
    }

    private void registerBindings() {
        titleLabel.textProperty().bind(getSkinnable().titleProperty());
        glyph.glyphNameProperty().bind(getSkinnable().glyphNameProperty());
        glyph.glyphSizeProperty().bind(getSkinnable().glyphSizeProperty());
        valueLabel.textProperty().bind(getSkinnable().valueProperty());
        unitLabel.textProperty().bind(getSkinnable().unitProperty());
        titleBox.alignmentProperty().bind(getSkinnable().titleAlignmentProperty());
        valueBox.alignmentProperty().bind(getSkinnable().valueAlignmentProperty());
    }

    private double getEmValue(String em) {
        return Double.parseDouble(em.replace("em", ""));
    }

    private void adjustFonts() {
        String fontSize = getSkinnable().getFontSize();
        if (fontSize.endsWith("em")) {
            double s = getEmValue(fontSize);
            valueLabel.setStyle("-fx-font-size: " + (s * 1.4) + "em;");
            unitLabel.setStyle("-fx-font-size: " + (s * 0.8) + "em;");
            titleLabel.setStyle("-fx-font-size: " + (s * 0.8) + "em;");
        }
    }

}
