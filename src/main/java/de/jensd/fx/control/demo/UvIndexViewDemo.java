package de.jensd.fx.control.demo;

import de.jensd.fx.control.UVIndexView;
import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Slider;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 *
 * @author Jens Deters (mail@jensd.de)
 */
public class UvIndexViewDemo extends Application {

    @Override
    public void start(Stage stage) throws Exception {

        VBox root = new VBox();
        root.setPadding(new Insets(20.0));

        UVIndexView uvIndexView = new UVIndexView();
        uvIndexView.setValue(5);
        
        VBox.setVgrow(uvIndexView, Priority.ALWAYS);
        
        final Slider slider = new Slider(-1.0, 11.0, 0.0);
        uvIndexView.valueProperty().bind(slider.valueProperty());
        root.getChildren().addAll(uvIndexView,slider);

        uvIndexView.valueProperty().addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
            System.out.println(slider.getValue() + " -- " + newValue);
        });
        
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

}
