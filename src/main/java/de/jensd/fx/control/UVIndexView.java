package de.jensd.fx.control;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.scene.control.Control;

/**
 *
 * @author Jens Deters
 */
public class UVIndexView extends Control{

    private IntegerProperty value;
    

    public UVIndexView(){
        getStyleClass().addAll("uvindex-view");
    }
    
    public IntegerProperty valueProperty() {
        if(value == null){
            value = new SimpleIntegerProperty();
        }
        return value;
    }

    
    public Integer getValue() {
        return valueProperty().get();
    }

    public void setValue(Integer value) {
        this.valueProperty().set(value);
    }
    
     @Override
    public String getUserAgentStylesheet() {
        return getClass().getResource(getClass().getSimpleName().toLowerCase() + ".css").toExternalForm();
    }
    
    
}
