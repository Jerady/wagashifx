package de.jensd.fx.demo.valueview;

import de.jensd.fx.control.ValueView;
import de.jensd.fx.glyphs.weathericons.WeatherIcon;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 *
 * @author Jens Deters (mail@jensd.de)
 */
public class ValueViewDemo extends Application {

    @Override
    public void start(Stage stage) throws Exception {

        VBox root = new VBox();

        ValueView valueView = new ValueView();
        valueView.setGlyphName(WeatherIcon.DAY_FOG.name());
        valueView.setValue("37.4");
        valueView.setUnit("\u2103");
        valueView.setFontSize("2em");
        valueView.setGlyphSize(48.0);
        valueView.setTitle("Temperature Outdoor");
        
        VBox.setVgrow(valueView, Priority.ALWAYS);
        root.getChildren().add(valueView);

        
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

}
